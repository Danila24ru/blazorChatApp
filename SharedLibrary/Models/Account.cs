﻿using System;

namespace BlazorApp1.Models
{
    public class Account
    {
        public string PlayerId { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public DateTime JoinedDate { get; set; }
    }
}
