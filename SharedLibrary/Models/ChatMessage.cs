﻿using System;

namespace BlazorApp1.Models
{
    public class ChatMessage
    {
        public string ChannelId { get; set; }
        public string FromId { get; set; }
        public string ToId { get; set; }
        public string Message { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
