﻿using BlazorApp1.Models;
using System.Collections.Generic;

namespace BlazorApp1.Models
{
    public class ChatState
    {
        public Clan Clan { get; set; }
        public List<ChatMessage> ChatMessages { get; set; }
        public List<PlayerData> Players { get; set; }
    }
}
