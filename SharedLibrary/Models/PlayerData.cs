﻿namespace BlazorApp1.Models
{
    public class PlayerData
    {
        public string PlayerId { get; set; }
        public string Name { get; set; }
        public string ClanId { get; set; }
    }
}
