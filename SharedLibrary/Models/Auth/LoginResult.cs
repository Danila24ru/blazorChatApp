﻿namespace BlazorApp1.Models
{
    public class LoginResult
    {
        public string Message { get; set; }
        public string AccessToken { get; set; }
        public bool Success { get; set; }
    }
}
