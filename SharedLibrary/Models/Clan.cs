﻿using System.Collections.Generic;

namespace BlazorApp1.Models
{
    public class Clan
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int MaxPlayersAmount { get; set; }

        public string OwnerId { get; set; }
        public List<string> PlayersIds { get; set; }
    }
}
