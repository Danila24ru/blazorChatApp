﻿using BlazorApp1.Clients;
using BlazorApp1.Models;
using BlazorApp1.Services;
using Microsoft.AspNetCore.Components;

namespace BlazorApp1.Pages
{
    public partial class ClanChat
    {
        [Parameter] public string ChannelId { get; set; }

        [Inject] IServiceProvider serviceProvider { get; set; }
        [Inject] ILogger<ClanChat> Logger { get; set; }
        [Inject] UserSession userSession { get; set; }

        private string CurrentMessage { get; set; }

        private Clan clan;

        private List<PlayerData> players = new List<PlayerData>();

        private List<ChatMessage> messages = new List<ChatMessage>();

        private ClanChatClient chatClient;
        
        protected override async Task OnInitializedAsync()
        {
            chatClient = serviceProvider.GetRequiredService<ClanChatClient>();

            chatClient.RecieveClanMessage(OnMessageArrived);
            chatClient.RecieveChatState(OnChatStateUpdated);
            chatClient.RecievePlayerJoined(OnPlayerJoined);
            chatClient.RecievePlayerLeaved(OnPlayerLeaved);

            if (chatClient.IsConnected)
            {
                await chatClient.RequestChatStateUpdate(ChannelId);
                return;
            }

            var isChatConnected = await chatClient.Connect(ChannelId);

            Logger.LogInformation($"Chat initialized! Connected: {isChatConnected}");

            if (isChatConnected)
            {
                await chatClient.EnterChat(ChannelId);
            }
        }

        private void OnPlayerLeaved(PlayerData player)
        {
            players.Remove(player);
        }

        private void OnPlayerJoined(PlayerData player)
        {
            players.Add(player);
        }

        private void OnChatStateUpdated(ChatState chatState)
        {
            messages = chatState.ChatMessages;
            players = chatState.Players;
            clan = chatState.Clan;

            StateHasChanged();
        }

        private void OnMessageArrived(ChatMessage message)
        {
            messages.Add(message);
            StateHasChanged();
        }

        private async Task SubmitAsync()
        {
            if (string.IsNullOrEmpty(CurrentMessage) || string.IsNullOrEmpty(ChannelId))
                return;
             
            var message = new ChatMessage()
            {
                FromId = await userSession.GetPlayerId(),
                Message = CurrentMessage,
                ChannelId = ChannelId,
                CreatedAt = DateTime.UtcNow,
            };

            messages.Add(message);
            await chatClient.SendMessage(message);
            CurrentMessage = string.Empty;
        }
    }
}
