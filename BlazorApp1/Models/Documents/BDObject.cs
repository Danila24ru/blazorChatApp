﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BlazorApp1.Models
{
    public class BDObject<TData> where TData : class
    {
        [BsonId]
        public string Id { get; set; }

        public TData data { get; set; }
    }
}
