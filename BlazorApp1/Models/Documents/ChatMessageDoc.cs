﻿using MongoDB.Bson.Serialization.Attributes;

namespace BlazorApp1.Models
{
    [BsonIgnoreExtraElements]
    public class ChatMessageDoc : BDObject<ChatMessage> { }
}
