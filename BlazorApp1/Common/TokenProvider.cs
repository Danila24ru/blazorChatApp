﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace BlazorApp1.Common
{
    public class TokenProvider
    {
		private const string KeySecret = "SuperSecret123456789!@#$%^&*(";
		public static SymmetricSecurityKey SecurityKey = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(KeySecret));

		public string CreateJWT(string playerId)
		{
			var credentials = new SigningCredentials(SecurityKey, SecurityAlgorithms.HmacSha256);

			var claims = new[]
			{
				new Claim(ClaimTypes.Name, playerId),
			};

			var token = new JwtSecurityToken(
				issuer: "domain.com",
				audience: "domain.com",
				claims: claims,
				expires: DateTime.Now.AddMinutes(60),
				signingCredentials: credentials);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}
	}
}
