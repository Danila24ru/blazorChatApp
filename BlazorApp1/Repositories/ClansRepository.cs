﻿using BlazorApp1.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BlazorApp1.Repositories
{
    public class ClanCreationResult
    {
        public ClanDoc Clan { get; set; }
        public bool IsSuccess { get; set; }
    }

    public class ClansRepository
    {
        private const string DocumentName = "clans";
        
        private readonly IMongoCollection<ClanDoc> clans;
        private readonly IMongoCollection<PlayerDoc> players;

        public ClansRepository(IMongoDatabase database)
        {
            clans = database.GetCollection<ClanDoc>(DocumentName);
            players = database.GetCollection<PlayerDoc>("players");
        }

        public async Task<List<ClanDoc>> GetClansAsync()
            => await clans.Find(x => true).ToListAsync();

        public async Task<long> GetClansCountAsync()
           => await clans.CountDocumentsAsync(x => true);

        public async Task<ClanDoc> GetClanAsync(string clanId)
            => await clans.Find(x => x.Id == clanId).FirstOrDefaultAsync();
        
        public Task<bool> IsClanExists(string name)
            => clans.Find(x => x.data.Name == name).AnyAsync();

        public async Task<bool> IsClanContainsPlayer(string clanId, string playerId)
            => await clans.Find(x => x.Id == clanId && x.data.PlayersIds.Contains(playerId)).AnyAsync();
        
        public async Task<ClanCreationResult> CreateClan(string name, string ownerPlayerId)
        {
            if (await IsClanExists(name))
                return new ClanCreationResult() { IsSuccess = false };

            var clanId = ObjectId.GenerateNewId().ToString();
            var clan = new ClanDoc()
            {
                Id = clanId,
                data = new Clan()
                {
                    Id = clanId,
                    Name = name,
                    OwnerId = ownerPlayerId,
                    PlayersIds = new List<string>() { ownerPlayerId }
                }
            };

            await clans.InsertOneAsync(clan);

            await SetPlayerClanId(ownerPlayerId, clan.Id);
            
            return new ClanCreationResult() { IsSuccess = true, Clan = clan };
        }

        private async Task SetPlayerClanId(string playerId, string clanId)
        {
            await players.UpdateOneAsync(
                Builders<PlayerDoc>.Filter.Eq(x => x.Id, playerId),
                Builders<PlayerDoc>.Update.Set(x => x.data.ClanId, clanId));
        }

        public async Task AddPlayerToClan(string clanId, string playerId)
        {
            var filter = Builders<ClanDoc>.Filter.Eq(x => x.Id, clanId);
            var update = Builders<ClanDoc>.Update.AddToSet(x => x.data.PlayersIds, playerId);

            await SetPlayerClanId(playerId, clanId);

            await clans.UpdateOneAsync(filter, update);
        }

        public async Task RemovePlayerFromClan(string clanId, string playerId)
        {
            var filter = Builders<ClanDoc>.Filter.Eq(x => x.Id, clanId);
            var update = Builders<ClanDoc>.Update.Pull(x => x.data.PlayersIds, playerId);
            
            await SetPlayerClanId(playerId, null);

            var result = await clans.UpdateOneAsync(filter, update);
        }
    }
}
