﻿using BlazorApp1.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BlazorApp1.Repositories
{
    public class MessagesRepository
    {
        private const string DocumentName = "messages";

        private IMongoCollection<ChatMessageDoc> messages;

        public MessagesRepository(IMongoDatabase database)
        {
            messages = database.GetCollection<ChatMessageDoc>(DocumentName);

            var indexes = Builders<ChatMessageDoc>.IndexKeys.Ascending(x => x.data.ChannelId);
            messages.Indexes.CreateOne(new CreateIndexModel<ChatMessageDoc>(indexes));
        }

        public void Add(ChatMessage message)
        {
            messages.InsertOne(new ChatMessageDoc() 
            {
                Id = ObjectId.GenerateNewId().ToString(),
                data = message
            });
        }

        public async Task<List<ChatMessageDoc>> GetMessages(string channelId, int maxMessages = 50)
        {
            var result = await messages
                .Find(x => x.data.ChannelId == channelId)
                .SortByDescending(x => x.Id)
                .Limit(maxMessages)
                .ToListAsync();
            
            result.Reverse();
            return result;
        }
    }
}
