﻿using BlazorApp1.Common;
using BlazorApp1.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace BlazorApp1.Repositories
{

    public class AccountsRepository
    {
        private readonly IMongoCollection<AccountDoc> accounts;
        private readonly IMongoCollection<PlayerDoc> players;

        public AccountsRepository(IMongoDatabase database)
        {
            accounts = database.GetCollection<AccountDoc>("accounts");
            players = database.GetCollection<PlayerDoc>("players");
        }

        public async Task<bool> IsAccountExists(LoginRequest loginModel)
        {
            var account = await accounts.Find(x => x.data.UserName == loginModel.UserName).FirstOrDefaultAsync();
            return account != null;
        }

        public async Task<AccountDoc> GetAccount(LoginRequest loginModel)
        {
            return await accounts.Find(
                x => x.data.UserName == loginModel.UserName && x.data.PasswordHash == Hashing.CreateMD5(loginModel.Password))
                .FirstOrDefaultAsync();
        }

        public async Task<List<PlayerData>> GetPlayers(List<string> playerIds)
        {
            var playerDocs = await players.Find(Builders<PlayerDoc>.Filter.In(x => x.Id, playerIds)).ToListAsync();
            return playerDocs.Select(x => x.data).ToList();
        }

        public async Task<PlayerDoc> GetPlayer(string playerId) =>
            await players.Find(x => x.Id == playerId)
                .FirstOrDefaultAsync();
        
        public AccountDoc CreateAccount(LoginRequest loginModel)
        {
            var accountId = ObjectId.GenerateNewId().ToString();
            var playerId = ObjectId.GenerateNewId().ToString();

            var account = new AccountDoc()
            {
                Id = accountId,
                data = new Account()
                {
                    PlayerId = playerId,
                    UserName = loginModel.UserName,
                    PasswordHash = Hashing.CreateMD5(loginModel.Password),
                    JoinedDate = DateTime.UtcNow,
                }
            };

            var player = new PlayerDoc()
            {
                Id = playerId,
                data = new PlayerData()
                {
                    PlayerId = playerId,
                    Name = loginModel.UserName,
                    ClanId = null,
                }
            };

            accounts.InsertOne(account);
            players.InsertOne(player);

            return account;
        }
    }
}
