﻿using BlazorApp1.Common;
using BlazorApp1.Models;
using BlazorApp1.Repositories;
using Microsoft.AspNetCore.Components.Server.ProtectedBrowserStorage;

namespace BlazorApp1.Services
{
	public class UserSession
	{
        private readonly ProtectedSessionStorage protectedSessionStorage;

        public UserSession(ProtectedSessionStorage protectedSessionStorage)
        {
            this.protectedSessionStorage = protectedSessionStorage;
        }

		public async void SetValue(string name, string value) => await protectedSessionStorage.SetAsync(name, value);

		public async Task<string> GetAccessToken() => (await protectedSessionStorage.GetAsync<string>("accessToken")).Value;
		public async Task<string> GetPlayerId() => (await protectedSessionStorage.GetAsync<string>("playerId")).Value;

		public async Task<bool> IsAuthenticated() => !string.IsNullOrEmpty(await GetAccessToken());
	}

    public class AuthService
    {
        private readonly AccountsRepository accountsRepository;
        private readonly TokenProvider tokenProvider;
        private readonly UserSession userSession;

        public AuthService(AccountsRepository accountsRepository, TokenProvider tokenProvider, UserSession userSession)
        {
            this.accountsRepository = accountsRepository;
            this.tokenProvider = tokenProvider;
            this.userSession = userSession;
        }

		public async Task<LoginResult> Register(LoginRequest loginData)
		{
			var isAlreadyExists = await accountsRepository.IsAccountExists(loginData);
			if (isAlreadyExists)
				return new LoginResult { Message = "Username already exists!", Success = false };

			var account = accountsRepository.CreateAccount(loginData);

			var accessToken = Authenticate(account.data.PlayerId);

			return new LoginResult { Message = $"Account created {account.data.UserName}", AccessToken = accessToken, Success = true };
		}

		public async Task<LoginResult> Login(LoginRequest loginData)
		{
			var account = await accountsRepository.GetAccount(loginData);
			if (account == null)
				return new LoginResult { Message = "Wrong login/password", Success = false };

			var accessToken = Authenticate(account.data.PlayerId);

			return new LoginResult { Message = $"Login success {account.data.UserName}", AccessToken = accessToken, Success = true };
		}

		private string Authenticate(string playerId)
        {
			var accessToken = tokenProvider.CreateJWT(playerId);

			userSession.SetValue(nameof(accessToken), accessToken);
			userSession.SetValue(nameof(playerId), playerId);

			return accessToken;
		}
	}
}
