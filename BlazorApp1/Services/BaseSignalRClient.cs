﻿using BlazorApp1.Services;
using Microsoft.AspNetCore.SignalR.Client;

namespace BlazorApp1.Clients
{
    public class BaseSignalRClient : IDisposable
    {
        private readonly UserSession userSession;

        protected HubConnection HubConnection { get; private set; }

        public string ChannelId { get; private set; }

        public bool IsConnected =>
            HubConnection?.State == HubConnectionState.Connected;

        public BaseSignalRClient(string url, UserSession userSession)
        {
            HubConnection = new HubConnectionBuilder()
                .WithUrl(url, (options) =>
                {
                    options.AccessTokenProvider = () => userSession.GetAccessToken();
                    options.SkipNegotiation = true;
                    options.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
                })
                .Build();
            this.userSession = userSession;
        }

        public async Task<bool> Connect(string channelId)
        {
            if (string.IsNullOrEmpty(await userSession.GetAccessToken()))
            {
                throw new Exception("Not authenticated!");
                return false;
            }
            
            this.ChannelId = channelId;

            await HubConnection.StartAsync();

            return true;
        }

        public void Dispose()
        {
            _ = HubConnection?.DisposeAsync();
        }
    }


}
