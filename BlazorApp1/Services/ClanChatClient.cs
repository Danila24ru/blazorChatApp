﻿using BlazorApp1.Hubs;
using BlazorApp1.Models;
using BlazorApp1.Services;
using Microsoft.AspNetCore.SignalR.Client;

namespace BlazorApp1.Clients
{
    public class ClanChatClient : BaseSignalRClient, IClanHubListener, IClanHubRequests
    {
        private readonly ILogger<ClanChatClient> logger;

        public ClanChatClient(UserSession userSession, IConfiguration configuration, ILogger<ClanChatClient> logger) 
            : base(configuration["App:SignalR:ClanHubHost"], userSession)
        {
            this.logger = logger;
            
        }

        public void RecieveClanMessage(Action<ChatMessage> action)
            => HubConnection.On(nameof(RecieveClanMessage), action);
        
        public void RecievePlayerJoined(Action<PlayerData> action)
            => HubConnection.On(nameof(RecievePlayerJoined), action);
        
        public void RecievePlayerLeaved(Action<PlayerData> action)
            => HubConnection.On(nameof(RecievePlayerLeaved), action);
        
        public void RecieveChatState(Action<ChatState> action)
            => HubConnection.On(nameof(RecieveChatState), action);

        public async Task EnterChat(string clanId)
        {
            if (!IsConnected) return;
            await HubConnection.SendAsync(nameof(EnterChat), clanId);
        }

        public async Task LeaveChat(string clanId)
        {
            if (!IsConnected) return;
            await HubConnection.SendAsync(nameof(LeaveChat), clanId);
        }

        public async Task SendMessage(ChatMessage message)
        {
            if (!IsConnected) return;
            await HubConnection.SendAsync(nameof(SendMessage), message);
        }

        public async Task RequestChatStateUpdate(string clanId)
        {
            if (!IsConnected) return;
            await HubConnection.SendAsync(nameof(RequestChatStateUpdate), clanId);
        }
    }


}
