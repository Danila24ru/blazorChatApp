﻿using BlazorApp1.Models;
using BlazorApp1.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace BlazorApp1.Hubs
{
    public interface IClanHubRequests
    {
        Task EnterChat(string clanId);
        Task LeaveChat(string clanId);
        Task SendMessage(ChatMessage message);
        Task RequestChatStateUpdate(string clanId);
    }

    public interface IClanHubClient
    {
        Task RecieveClanMessage(ChatMessage chatMessage);
        Task RecievePlayerJoined(PlayerData player);
        Task RecievePlayerLeaved(PlayerData player);
        Task RecieveChatState(ChatState chatState);
    }

    public interface IClanHubListener
    {
        void RecieveClanMessage(Action<ChatMessage> action);
        void RecievePlayerJoined(Action<PlayerData> action);
        void RecievePlayerLeaved(Action<PlayerData> action);
        void RecieveChatState(Action<ChatState> action);
    }

    [Authorize]
    public class ClanHub : Hub<IClanHubClient>, IClanHubRequests
    {
        private readonly MessagesRepository clanMessages;
        private readonly AccountsRepository accountsRepository;
        private readonly ClansRepository clansRepository;
        private readonly ILogger<ClanHub> logger;

        private PlayerDoc player;

        public ClanHub(MessagesRepository clanMessages, AccountsRepository accountsRepository, ClansRepository clansRepository, ILogger<ClanHub> logger)
        {
            this.clanMessages = clanMessages;
            this.accountsRepository = accountsRepository;
            this.clansRepository = clansRepository;
            this.logger = logger;
        }

        public async Task EnterChat(string clanId)
        {
            var playerId = Context.User.Identity.Name;

            if (!await clansRepository.IsClanContainsPlayer(clanId, playerId))
                return;

            player = await accountsRepository.GetPlayer(playerId);

            await Groups.AddToGroupAsync(Context.ConnectionId, clanId);

            NotifyPlayerJoined(player, clanId);
            await SendChatState(clanId);
        }

        public async Task LeaveChat(string clanId)
        {
            await Clients.GroupExcept(clanId, Context.ConnectionId).RecievePlayerLeaved(player.data);

            await Groups.RemoveFromGroupAsync(Context.ConnectionId, clanId);
        }

        public async Task RequestChatStateUpdate(string clanId)
        {
            await SendChatState(clanId);
        }

        public async Task SendMessage(ChatMessage message)
        {
            message.FromId = Context.User.Identity.Name; 

            clanMessages.Add(message);

            await Clients.GroupExcept(message.ChannelId, Context.ConnectionId).RecieveClanMessage(message);
        }

        private async void NotifyPlayerJoined(PlayerDoc player, string clanId)
        {
            logger.LogInformation($"Player: {player.data.Name} entered chat: {clanId}");

            await Clients.GroupExcept(clanId, Context.ConnectionId).RecievePlayerJoined(player.data);
        }

        private async Task SendChatState(string clanId)
        {
            var messages = await clanMessages.GetMessages(clanId);
            var clan = await clansRepository.GetClanAsync(clanId);

            var chatState = new ChatState()
            {
                ChatMessages = messages.Select(x => x.data).ToList(),
                Players = await accountsRepository.GetPlayers(clan.data.PlayersIds),
                Clan = clan.data
            };

            await Clients.Caller.RecieveChatState(chatState);
        }
    }
}
