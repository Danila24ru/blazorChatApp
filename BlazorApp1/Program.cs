using BlazorApp1.Hubs;
using BlazorApp1.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using MudBlazor.Services;
using MongoDB.Driver;
using BlazorApp1.Repositories;
using BlazorApp1.Clients;
using BlazorApp1.Common;

var builder = WebApplication.CreateBuilder(args);

var config = builder.Configuration;

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();
builder.Services.AddSignalR()
                .AddStackExchangeRedis(config["DB:Redis:Host"], options => 
                {
                    options.Configuration.ChannelPrefix = config["DB:Redis:ChannelPrefix"];
                });
builder.Services.AddMudServices();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer("Bearer", options =>
{
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateAudience = true,
        ValidAudience = "domain.com",
        ValidateIssuer = true,
        ValidIssuer = "domain.com",
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = TokenProvider.SecurityKey
	};
    options.Events = new JwtBearerEvents
    {
        OnMessageReceived = context =>
        {
            var accessToken = context.Request.Query["access_token"];

            var path = context.HttpContext.Request.Path;
            if (!string.IsNullOrEmpty(accessToken) &&
                (path.StartsWithSegments("/clanHub")))
            {
                context.Token = accessToken;
            }
            return Task.CompletedTask;
        }
    };
});
builder.Services.AddAuthorization();

builder.Services.AddSingleton<AccountsRepository>();
builder.Services.AddSingleton<MessagesRepository>();
builder.Services.AddSingleton<ClansRepository>();
builder.Services.AddSingleton<TokenProvider>();
builder.Services.AddScoped<UserSession>();

builder.Services.AddScoped<AuthService>();
builder.Services.AddScoped<ClanChatClient>();

var mongoClient = new MongoClient(config["DB:MongoDB:Host"]);
var currentDB = mongoClient.GetDatabase(config["DB:MongoDB:DatabaseName"]);
builder.Services.AddSingleton<IMongoDatabase>(currentDB);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseStaticFiles();

app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");
app.MapHub<ClanHub>("/clanHub");

app.Run();
