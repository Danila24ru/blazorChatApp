# BlazorChatApp
Простой чат проект с комантами (кланами).
Возможности:
 - логин/регистрация
 - создание клана / поиск клана / возможность зайти в другой клан
 - чат клана

TechStack: 
NET 6.0, Blazor Server, SignalR, MongoDB, Redis, Docker-Compose, NGINX.

Как запустить?
Через visual studio -> docker-compose -> play.
Либо через консоль docker compose up

Замечания:
- HTTPS не настроен! Запускать через http://localhost/
- 1 вкладка браузера == 1 клиент (сессия).
- Frontend содержит некоторые косяки.
- В docker-compose.yml запускается два инстанса сервера чата blazorapp1, blazorapp2.